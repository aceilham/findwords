package com.ace.findwords

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView


class HurufAdapter
    : RecyclerView.Adapter<HurufAdapter.HurufViewHolder>() {

    // Membuat daftar huruf dari A sampai J
    private val list =('A').rangeTo('J').toList()

    class HurufViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val button = view.findViewById<Button>(R.id.btnItem)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HurufViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_view, parent, false)
        return HurufViewHolder(view)
    }

    override fun onBindViewHolder(holder: HurufViewHolder, position: Int) {
        val item = list.get(position)
        holder.button.text = item.toString()

        // Navigate ke Fragment Kata
        holder.button.setOnClickListener {
            val action = ListHurufFragmentDirections.actionListHurufFragmentToListKataFragment(holder.button.text.toString())
            holder.view.findNavController().navigate(action)
        }
    }
    override fun getItemCount(): Int {
        return list.size
    }
}
