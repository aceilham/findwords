package com.ace.findwords

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.view.accessibility.AccessibilityNodeInfo
import android.widget.Button
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView


class KataAdapter(private val idHuruf: String, context: Context) :
    RecyclerView.Adapter<KataAdapter.KataViewHolder>() {
    private val filteredWords: List<String>

    init {
        // Mengambil daftar kata dari array kata
        val words = context.resources.getStringArray(R.array.kata).toList()

        filteredWords = words
            // Menyaring kata dimulai dari huruf yang diinput
            .filter { it.startsWith(idHuruf, ignoreCase = true) }
            // Dari kata yang diambil akan diacak
            .shuffled()
            // Diambil 3 kata dari daftar kata di array
            .take(3)
            // Daftar kata yang telah diambil diurutkan
            .sorted()
    }

    class KataViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val button = view.findViewById<Button>(R.id.btnItem)
    }

    override fun getItemCount(): Int= filteredWords.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KataViewHolder {
        val view = LayoutInflater.from(parent.context)
        .inflate(R.layout.item_view, parent, false)
        return KataViewHolder(view)
    }

    override fun onBindViewHolder(holder: KataViewHolder, position: Int) {
        val item = filteredWords[position]
        val context = holder.view.context
        holder.button.text = item

        // Implementasi intent untuk mencari kata yang diinput
        holder.button.setOnClickListener {
            val queryUrl: Uri = Uri.parse("${ListKataFragment.SEARCH_PREFIX}${item}")
            val intent = Intent(Intent.ACTION_VIEW, queryUrl)
            context.startActivity(intent)
        }
    }
}